package com.gitee.occo.exchange.entity;

import com.gitee.occo.exchange.common.entity.Order;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FundOperatesDto {
	
	/**
	 * -用户ID
	 */
	private long uid;
	
	/**
	 * -币
	 */
	private String coin;
	
	/**
	 * -数量
	 */
	private long number;
	
	public static FundOperatesDto getFundOperatesDto(Order order) {
		long number = order.getNumber();
		if (order.isBid()) {
			number = order.getAmount();
		}
		String coin = "BTC";
		return new FundOperatesDto(order.getUid(),coin, number);
	}
}
